const token = localStorage.getItem(`token`)
const isAdmin = localStorage.getItem("admin")
const courseContainer = document.getElementById(`anyContainer`)
const adminButton = document.getElementById(`adminButton`)
let c
let courses;

console.log(isAdmin == false)
console.log(typeof isAdmin)
console.log(typeof "false")

if(isAdmin == false){
    //regular user
    // send request to get all active courses
    fetch(`http://localhost:3008/api/courses/isActive`, {
        method: "GET",
        headers: {
            "Authorization": `Bearer ${token}`
        }
})

// wait for server's response
.then(result => result.json())
.then(result => {
    console.log(result)
    if (result.length < 1){
        return `No courses available.`
    } else {
        courses = result.map(course => {
            const {courseName, description, price} = course
            return (
                `
                    <div class="col-12 col-md-4 my-2">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">
                                ${courseName}
                            </h5>
                            <p class="card-text text-left">
                                ${description}
                            </p>
                            <p class="card-text text-right">
                                ${price}
                            </p>
                            <div class="card-footer>
                            </div>
                        </div>
                    </div>
                    </div>
                `
            )
        }).join (" ")
        //console.log(courses)
        courseContainer.innerHTML = courses
    }
})
} else {
    //admin

    // show the button if it is admin
    adminButton.innerHTML = 
    `
		<a class="btn btn-info m-3" href="./addCourse.html">Add Course</a>
		<a class="btn btn-info m-3" href="./dashboard.html">Dashboard</a>

	`

    // show all the courses in the Course Page
        // both offered and not 
}